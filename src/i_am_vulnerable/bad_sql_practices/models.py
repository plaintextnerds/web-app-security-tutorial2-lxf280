from django.db import models


class Question(models.Model):
    question_text: models.CharField = models.CharField(max_length=200)
    pub_date: models.DateTimeField = models.DateTimeField("date published")

    def __str__(self):
        """Returns a formatted string representation."""
        return f"{self.question_text} - [{', '.join(choice.choice_text for choice in self.choice_set.all())}]"


class Choice(models.Model):
    question: models.ForeignKey = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text: models.CharField = models.CharField(max_length=200)
    votes: models.IntegerField = models.IntegerField(default=0)

    def __str__(self):
        """Returns a formatted string representation."""
        return f"{self.question.question_text} - {self.choice_text}"


class Vote(models.Model):
    question: models.ForeignKey = models.ForeignKey(Question, on_delete=models.CASCADE)
    email: models.TextField = models.TextField()

    def __str__(self):
        """Returns a formatted string representation."""
        return f"{self.email} - {self.question.question_text}"
