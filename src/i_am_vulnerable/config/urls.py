"""i_am_vulnerable URL Configuration."""

from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("bad_sql/", include("bad_sql_practices.urls")),
    path("admin/", admin.site.urls),
]
